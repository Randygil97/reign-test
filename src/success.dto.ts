export interface SuccessDto {
  success: boolean;
  message?: string;
  data?: any;
}
