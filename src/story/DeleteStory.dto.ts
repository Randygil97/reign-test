import { ApiProperty } from '@nestjs/swagger';

export class DeleteStoryDTO {
  @ApiProperty({ required: true })
  id?: string;
}
