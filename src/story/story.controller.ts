import {
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  Query,
  UseGuards,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';
import { SuccessDto } from '../success.dto';
import { Pagination } from './../paginate';
import { DeleteStoryDTO } from './DeleteStory.dto';
import { QueryStoriesDto } from './queryStories.dto';
import { Story } from './story.entity';
import { StoryService } from './story.service';

@Controller('story')
export class StoryController {
  constructor(private storyService: StoryService) {}

  @Get()
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(ClassSerializerInterceptor)
  @UsePipes(new ValidationPipe({ transform: true }))
  async index(@Query() query: QueryStoriesDto): Promise<Pagination<Story>> {
    const items = await this.storyService.findAll(
      {
        limit: query.hasOwnProperty('limit') ? query.limit : 5,
        page: query.hasOwnProperty('page') ? query.page : 1,
      },
      query,
    );
    return items;
  }

  @Delete(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  async delete(@Param() param: DeleteStoryDTO): Promise<SuccessDto> {
    await this.storyService.delete(param.id);
    return { success: true };
  }
}
