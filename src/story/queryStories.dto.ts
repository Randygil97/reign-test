import { Type } from 'class-transformer';
import { IsIn, IsNumber, IsOptional } from 'class-validator';
import { months } from './month.definitions';
import { ApiProperty } from '@nestjs/swagger';

export class QueryStoriesDto {
  @IsIn(Object.keys(months))
  @IsOptional()
  @ApiProperty({ required: false })
  month?: string;

 @ApiProperty({ required: false })
  @IsOptional()
  title?: string;

 @ApiProperty({ required: false })
  @IsOptional()
  author?: string;

 @ApiProperty({ required: false })
  @IsOptional()
  tag?: string;

 @ApiProperty({ required: false })
  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  limit?: number;

 @ApiProperty({ required: false })
  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  page?: number;
}
