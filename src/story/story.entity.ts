import { Transform } from 'class-transformer';
import {
  Column,
  Entity,
  OneToMany,
  PrimaryColumn,
  DeleteDateColumn,
} from 'typeorm';
import { Tag } from './tags.entity';

@Entity('stories')
export class Story {
  @PrimaryColumn()
  objectID: string;

  @DeleteDateColumn()
  deletedAt?: Date;

  @Column({ type: 'timestamptz', nullable: true })
  created_at: Date;

  @Column({ type: 'varchar', nullable: true })
  title: string;

  @Column({ type: 'varchar', nullable: true })
  url: string;

  @Column({ type: 'varchar', nullable: true })
  author: string;

  @Column({ type: 'int', nullable: true })
  points: number;

  @Column({ type: 'varchar', nullable: true })
  story_text: string;

  @Column({ type: 'varchar', nullable: true })
  comment_text: string;

  @Column({ type: 'varchar', nullable: true })
  num_comments: number;

  @Column({ type: 'varchar', nullable: true })
  story_title: string;

  @Column({ type: 'varchar', nullable: true })
  story_url: string;

  @Column({ type: 'int', nullable: true })
  created_at_i: number;

  @Transform(({ value }) => value.map(({ name }) => name))
  @OneToMany(() => Tag, (tag) => tag.story, { cascade: ['insert', 'update'] })
  tags: Tag[];
}
