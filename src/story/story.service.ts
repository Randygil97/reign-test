import {
  ClassSerializerInterceptor,
  HttpException,
  HttpStatus,
  Injectable,
  UseInterceptors,
} from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios';
import { Observable } from 'rxjs';
import { Pagination, PaginationOptionsInterface } from './../paginate';
import { months } from './month.definitions';
import { Story } from './story.entity';
import { Like, Repository, SelectQueryBuilder } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { QueryStoriesDto } from './queryStories.dto';
import { APIStoryDTO } from './APIStory.dto';
@Injectable()
export class StoryService {
  constructor(
    @InjectRepository(Story)
    private storyRepository: Repository<Story>,
    private httpService: HttpService,
  ) {}

  fetchStories(): Observable<AxiosResponse<any>> {
    const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
    return this.httpService.get(url);
  }

  @UseInterceptors(ClassSerializerInterceptor)
  async add(story: APIStoryDTO): Promise<Story> {
    const storyModel = await this.storyRepository.save(story);
    return storyModel;
  }
  @UseInterceptors(ClassSerializerInterceptor)
  async findAll(
    options: PaginationOptionsInterface,
    query: QueryStoriesDto,
  ): Promise<Pagination<Story>> {
    const { author, title, month, tag } = query;
    const monthInNumber = months[month];
    const [results, total] = await this.storyRepository.findAndCount({
      join: { alias: 'story', leftJoin: { tags: 'story.tags' } },
      take: options.limit,
      skip: (options.page - 1) * options.limit,
      relations: ['tags'],
      where: (qb: SelectQueryBuilder<any>) => {
        qb.where({ deletedAt: null });
        if (author) {
          qb.andWhere({ author: Like(`%${author}%`) });
        }
        if (title) {
          qb.andWhere({ title: Like(`%${title}%`) });
        }

        if (tag) {
          qb.andWhere('tags.name = :tag', { tag });
        }
        if (month) {
          console.log(month);
          qb.where('EXTRACT(MONTH FROM created_at) = :month', {
            month: monthInNumber,
          });
        }
      },
    });
    return new Pagination<Story>({
      results,
      total,
    });
  }

  @UseInterceptors(ClassSerializerInterceptor)
  async delete(id: string) {
    const story = await this.storyRepository.findOne(id);
    if (!story) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'Story not found',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    story.deletedAt = new Date();
    await this.storyRepository.save(story);
    return story;
  }
}
