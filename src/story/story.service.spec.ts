import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { of } from 'rxjs';
import { Story } from './story.entity';
import { StoryService } from './story.service';
import { AxiosResponse } from 'axios';
import { HttpModule, HttpService } from '@nestjs/axios';
import { plainToClass } from 'class-transformer';
import { Tag } from './tags.entity';
import { APIStoryDTO } from './APIStory.dto';

describe('StoryService', () => {
  let service: StoryService;
  let httpService: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        StoryService,
        {
          provide: getRepositoryToken(Story),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<StoryService>(StoryService);
    httpService = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return Dummy Data when fetch stories successfully', () => {
    const result: AxiosResponse = {
      data: ['data'],
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };
    jest.spyOn(httpService, 'get').mockImplementation(() => of(result));
    service.fetchStories().subscribe((res) => {
      expect(res).toEqual(result);
    });
  });

  it('should return Dummy Data when add story successfully', async () => {
    const tags = ['comment', 'author_quickthrower2', 'story_28657292'];
    const result: Story = {
      objectID: '28658631',
      deletedAt: null,
      created_at: new Date('2021-09-26T05:24:08.000Z'),
      title: null,
      url: null,
      author: 'quickthrower2',
      points: null,
      story_text: null,
      comment_text: '(but not always) &lt;~ this includes nodejs',
      num_comments: null,
      story_title: 'JSFuck (2012)',
      story_url: 'http://www.jsfuck.com/',
      created_at_i: 1632633848,
      tags: plainToClass(Tag, tags),
    };

    jest
      .spyOn(service, 'add')
      .mockImplementation(() => new Promise((resolve) => resolve(result)));

    const addStory: APIStoryDTO = {
      ...result,
      story_id: 'dummy',
      _tags: tags,
    };
    expect(await service.add(addStory)).toBe(result);
  });
});
