import { Test, TestingModule } from '@nestjs/testing';
import { StoryController } from './story.controller';
import { Story } from './story.entity';
import { StoryService } from './story.service';
import { HttpModule } from '@nestjs/axios';
import { plainToClass } from 'class-transformer';
import { getRepositoryToken } from '@nestjs/typeorm';
describe('StoryController', () => {
  let controller: StoryController;
  let storyService: StoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StoryController],
      imports: [HttpModule],
      providers: [
        StoryService,
        {
          provide: getRepositoryToken(Story),
          useValue: {},
        },
      ],
    }).compile();

    controller = module.get<StoryController>(StoryController);
    storyService = module.get<StoryService>(StoryService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll', () => {
    it('should return a paginated list of storys', async () => {
      const results = plainToClass(Story, [
        {
          objectID: '28658631',
          deletedAt: null,
          created_at: '2021-09-26T05:24:08.000Z',
          title: null,
          url: null,
          author: 'quickthrower2',
          points: null,
          story_text: null,
          comment_text: '(but not always) &lt;~ this includes nodejs',
          num_comments: null,
          story_title: 'JSFuck (2012)',
          story_url: 'http://www.jsfuck.com/',
          created_at_i: 1632633848,
          tags: ['comment', 'author_quickthrower2', 'story_28657292'],
        },
      ]);
      const result = {
        results,
        total: 20,
        page_total: 5,
      };
      const options = {
        limit: 1,
        page: 1,
      };

      const query = {};
      jest
        .spyOn(storyService, 'findAll')
        .mockImplementation(async () => result);

      expect(await storyService.findAll(options, query)).toBe(result);
    });
  });
});
