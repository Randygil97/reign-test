import { Module } from '@nestjs/common';
import { StoryService } from './story.service';
import { StoryController } from './story.controller';
import { Story } from './story.entity';
import { Tag } from './tags.entity';

import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';
@Module({
  imports: [HttpModule, TypeOrmModule.forFeature([Story, Tag])],
  exports: [StoryService],
  providers: [StoryService],
  controllers: [StoryController],
})
export class StoryModule {}
