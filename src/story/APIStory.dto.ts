export class APIStoryDTO {
  story_id: string;
  created_at: Date;
  title: string;
  url: string;
  author: string;
  points: number;
  story_text: string;
  comment_text: string;
  num_comments: number;
  story_title: string;
  story_url: string;
  objectID: string;
  created_at_i: number;
  _tags: string[];
  constructor(
    story_id: string,
    created_at: Date,
    title: string,
    url: string,
    author: string,
    points: number,
    story_text: string,
    comment_text: string,
    num_comments: number,
    story_title: string,
    story_url: string,
    created_at_i: number,
    _tags: string[],
  ) {
    this.story_id = story_id;
    this.created_at = created_at;
    this.title = title;
    this.url = url;
    this.author = author;
    this.points = points;
    this.story_text = story_text;
    this.comment_text = comment_text;
    this.num_comments = num_comments;
    this.story_title = story_title;
    this.story_url = story_url;
    this.created_at_i = created_at_i;
    this._tags = _tags;
  }
}
