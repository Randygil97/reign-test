import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Story } from './story.entity';

@Entity('tags')
export class Tag {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @ManyToOne(() => Story, (story) => story.tags)
  story: Story;
}
