import { ApiProperty } from '@nestjs/swagger';

export class UserDTO {
  readonly id?: string;

  @ApiProperty({ default: 'reign' })
  readonly name: string;

  @ApiProperty({ default: '123456' })
  readonly password?: string;

  constructor(id: string, name: string, password: string) {
    this.id = id;
    this.name = name;
    this.password = password;
  }
}
