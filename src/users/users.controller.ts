import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';
import { UserDTO } from '../users/users.dto';
import { Auth } from '../auth/auth.decorator';
import { UsersService } from './users.service';
import { User } from './users.entity';
import { SuccessDto } from '../success.dto';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get('/me')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(ClassSerializerInterceptor)
  async getUserById(@Auth() { id }: UserDTO): Promise<SuccessDto> {
    const user = await this.usersService.findOne(id);
    const { name, password } = user;
    const DTO = new User(id, name, password);
    return { success: true, data: DTO };
  }

  @Post('/register')
  @ApiBearerAuth()
  async register(@Body() { name, password }: UserDTO): Promise<UserDTO> {
    return await this.usersService.create(name, password);
  }
}
