import { Injectable, Logger } from '@nestjs/common';
import { Cron, Interval } from '@nestjs/schedule';
import { StoryService } from '../story/story.service';
@Injectable()
export class TasksService {
  constructor(
    // @InjectRepository(Story)
    private readonly storyService: StoryService,
  ) {}

  private readonly logger = new Logger(TasksService.name);

  @Cron('* 0 * * * *')
  async handleCron() {
    this.logger.debug('Fetching stories data');
    try {
      this.storyService.fetchStories().subscribe((data) => {
        data.data.hits.forEach((story: any) => {
          const { _tags } = story;

          story.tags = _tags.map((tag: any) => ({
            name: tag,
          }));
          delete story._tags;
          this.storyService.add(story);
        });
      });
    } catch (err: any) {
      console.log('Error', err);
    }
  }
}
