import { Module } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { HttpModule } from '@nestjs/axios';
import { StoryModule } from '../story/story.module';
import { Story } from '../story/story.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
@Module({
  providers: [TasksService],
  imports: [HttpModule, TypeOrmModule.forFeature([Story]), StoryModule],
})
export class TasksModule {}
