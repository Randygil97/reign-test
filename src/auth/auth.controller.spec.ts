import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersService } from '../users/users.service';
import { User } from '../users/users.entity';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { ConfigModule } from '@nestjs/config';

describe('AuthController', () => {
  let controller: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      imports: [
        ConfigModule.forRoot(),
        JwtModule.register({
          secret: 'testkey',
          signOptions: { expiresIn: '60h' },
        }),
        PassportModule,
      ],
      providers: [
        UsersService,
        AuthService,
        //   JwtStrategy,
        {
          provide: getRepositoryToken(User),
          useValue: {},
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll', () => {
    it('user can login', async () => {
      const result = { access_token: 'token' };
      jest
        .spyOn(authService, 'validateUser')
        .mockImplementation(() => new Promise((resolve) => resolve(true)));

      jest
        .spyOn(authService, 'generateAccessToken')
        .mockImplementation(() => new Promise((resolve) => resolve(result)));

      expect(await controller.login({ name: 'test', password: 'test' })).toBe(
        result,
      );
    });
  });
});
