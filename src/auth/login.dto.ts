import { ApiProperty } from '@nestjs/swagger';

export class LoginDTO {
  @ApiProperty({ default: 'reign' })
  name: string;

  @ApiProperty({ default: '123456' })
  password: string;
}
