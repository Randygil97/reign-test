"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs-extra");
const istanbul_api_1 = require("istanbul-api");
const istanbul_lib_coverage_1 = require("istanbul-lib-coverage");
const yargs = require("yargs");
main().catch((err) => {
    console.error(err);
    process.exit(1);
});
async function main() {
    const argv = yargs
        .options({
        report: {
            type: 'array',
            desc: 'Path of json coverage report file',
            demandOption: true,
        },
        reporters: {
            type: 'array',
            default: ['json', 'lcov', 'text'],
        },
    })
        .argv;
    const reportFiles = argv.report;
    const reporters = argv.reporters;
    const map = (0, istanbul_lib_coverage_1.createCoverageMap)({});
    reportFiles.forEach((file) => {
        const r = fs.readJsonSync(file);
        map.merge(r);
    });
    const reporter = (0, istanbul_api_1.createReporter)();
    await reporter.addAll(reporters);
    reporter.write(map);
    console.log('Created a merged coverage report in ./coverage');
}
//# sourceMappingURL=merge-coverage.js.map