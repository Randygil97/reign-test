#;
Reign;
Test;
This;
is;
a;
Back;
End;
Developer;
Test;
provided;
by[reign](https, #, #, Use
    - Start, the, mysql, container, using, docker `` `bash
$ docker-compose up -d 
` ``
    - Start, the, nestjs, process, using, to, following `` `bash
$ yarn start
` ``, #, #, #, Production, If, you, 're going to use this example in production (or your own verison of it) it', s, recommended, to, run, using, the, 'complied', JS, version, from, dist.You, can);
do
    this;
while (by);
using;
the;
following;
command `` `bash
$ yarn start:prod
` ``
    > This;
command;
will;
also;
clean;
and;
build;
your;
dist;
folder;
#;
#;
Development;
For;
development, the;
best;
command;
to;
use;
is `` `bash
$ yarn start:dev
` ``;
This;
will;
start;
nodemon;
to;
reload;
our;
script;
when;
there;
's been any changes in the src directory ;
#;
#;
Testing;
#;
#;
#;
#;
Unit;
testing;
Unit;
tests;
can;
be;
ran;
by;
simply;
using;
the `test`;
script `` `bash
$ yarn test
` ``;
This;
will;
run;
jest;
on;
all `.spec.ts`;
files.
;
#;
#;
#;
#;
End;
to;
End;
testing(E2E);
End;
to;
end;
tests;
can;
be;
run;
by;
using;
the;
following;
command `` `bash
$ yarn test:e2e
` ``;
this;
will;
run;
jest;
on;
all `.e2e-spec.ts`;
files.
;
#;
#;
#;
#;
Coverage;
Use;
jest;
to;
show;
you;
a;
coverage;
of;
your;
tests `` `bash
$ yarn test:cov
` ``;
#;
#;
Build;
your;
own;
NestJS;
application;
Want;
to;
get;
started;
on;
your;
own;
NestJS;
application ? Simply : ;
install;
the[nest - cli](https, #, Packages, I, used, a, variety, of, packages, to, develop, this, example, api.Here, 's a list of them and where I got them from 
    - Nestjs
    - []);
/typeorm](https:/ / github.com / nestjs / typeorm;
A;
typeorm;
module;
for (nestjs
    - []; ; )
/passport](https:/ / github.com / nestjs / passport;
An;
easy;
to;
use;
module;
for (passport; include; AuthGuards
    - [])
/jwt](https:/ / github.com / nestjs / jwt;
A;
JWT;
module;
for (nestjs
    - nestjs - community
    - [nestjs - config](https, -[typeorm](https)); ; )
    ;
//# sourceMappingURL=README.js.map