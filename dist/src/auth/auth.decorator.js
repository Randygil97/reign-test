"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Auth = void 0;
const common_1 = require("@nestjs/common");
exports.Auth = (0, common_1.createParamDecorator)((data, ctx) => {
    try {
        const request = ctx.switchToHttp().getRequest();
        return request.user;
    }
    catch (error) {
        throw new common_1.ForbiddenException();
    }
});
//# sourceMappingURL=auth.decorator.js.map