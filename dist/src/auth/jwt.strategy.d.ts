import { Strategy } from 'passport-jwt';
import { UserDTO } from '../users/users.dto';
import { UsersService } from '../users/users.service';
import { JWTPayload } from './jwt.payload';
declare const JwtStrategy_base: new (...args: any[]) => Strategy;
export declare class JwtStrategy extends JwtStrategy_base {
    private usersService;
    constructor(usersService: UsersService);
    validate(payload: JWTPayload): Promise<UserDTO>;
}
export {};
