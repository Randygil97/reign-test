import { UsersService } from '../users/users.service';
import { AuthService } from './auth.service';
import { LoginDTO } from './login.dto';
export declare class AuthController {
    private authService;
    private usersService;
    constructor(authService: AuthService, usersService: UsersService);
    login(loginDTO: LoginDTO): Promise<{
        access_token: string;
    }>;
}
