import { PaginationResultInterface } from './pagination.results.interface';
export declare class Pagination<PaginationEntity> {
    results: PaginationEntity[];
    page_total: number;
    total: number;
    constructor(paginationResults: PaginationResultInterface<PaginationEntity>);
}
