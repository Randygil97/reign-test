import { UserDTO } from '../users/users.dto';
import { UsersService } from './users.service';
import { SuccessDto } from '../success.dto';
export declare class UsersController {
    private usersService;
    constructor(usersService: UsersService);
    getUserById({ id }: UserDTO): Promise<SuccessDto>;
    register({ name, password }: UserDTO): Promise<UserDTO>;
}
