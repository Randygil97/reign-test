export declare class User {
    userId: string;
    name: string;
    password: string;
    hashPassword(): Promise<void>;
    validatePassword(password: string): Promise<boolean>;
    constructor(userId: string, name: string, pass: string);
}
