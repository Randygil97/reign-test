export declare class UserDTO {
    readonly id?: string;
    readonly name: string;
    readonly password?: string;
    constructor(id: string, name: string, password: string);
}
