import { StoryService } from '../story/story.service';
export declare class TasksService {
    private readonly storyService;
    constructor(storyService: StoryService);
    private readonly logger;
    handleCron(): Promise<void>;
}
