import { Story } from './story.entity';
export declare class Tag {
    id: string;
    name: string;
    story: Story;
}
