import { HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios';
import { Observable } from 'rxjs';
import { Pagination, PaginationOptionsInterface } from './../paginate';
import { Story } from './story.entity';
import { Repository } from 'typeorm';
import { QueryStoriesDto } from './queryStories.dto';
import { APIStoryDTO } from './APIStory.dto';
export declare class StoryService {
    private storyRepository;
    private httpService;
    constructor(storyRepository: Repository<Story>, httpService: HttpService);
    fetchStories(): Observable<AxiosResponse<any>>;
    add(story: APIStoryDTO): Promise<Story>;
    findAll(options: PaginationOptionsInterface, query: QueryStoriesDto): Promise<Pagination<Story>>;
    delete(id: string): Promise<Story>;
}
