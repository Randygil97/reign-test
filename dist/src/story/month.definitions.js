"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.months = void 0;
exports.months = {
    january: 1,
    february: 2,
    march: 3,
    april: 4,
    may: 5,
    june: 6,
    july: 7,
    august: 8,
    september: 9,
    october: 10,
    november: 11,
    december: 12,
};
//# sourceMappingURL=month.definitions.js.map