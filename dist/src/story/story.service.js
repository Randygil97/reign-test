"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StoryService = void 0;
const common_1 = require("@nestjs/common");
const axios_1 = require("@nestjs/axios");
const paginate_1 = require("./../paginate");
const month_definitions_1 = require("./month.definitions");
const story_entity_1 = require("./story.entity");
const typeorm_1 = require("typeorm");
const typeorm_2 = require("@nestjs/typeorm");
const queryStories_dto_1 = require("./queryStories.dto");
const APIStory_dto_1 = require("./APIStory.dto");
let StoryService = class StoryService {
    constructor(storyRepository, httpService) {
        this.storyRepository = storyRepository;
        this.httpService = httpService;
    }
    fetchStories() {
        const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
        return this.httpService.get(url);
    }
    async add(story) {
        const storyModel = await this.storyRepository.save(story);
        return storyModel;
    }
    async findAll(options, query) {
        const { author, title, month, tag } = query;
        const monthInNumber = month_definitions_1.months[month];
        const [results, total] = await this.storyRepository.findAndCount({
            join: { alias: 'story', leftJoin: { tags: 'story.tags' } },
            take: options.limit,
            skip: (options.page - 1) * options.limit,
            relations: ['tags'],
            where: (qb) => {
                qb.where({ deletedAt: null });
                if (author) {
                    qb.andWhere({ author: (0, typeorm_1.Like)(`%${author}%`) });
                }
                if (title) {
                    qb.andWhere({ title: (0, typeorm_1.Like)(`%${title}%`) });
                }
                if (tag) {
                    qb.andWhere('tags.name = :tag', { tag });
                }
                if (month) {
                    console.log(month);
                    qb.where('EXTRACT(MONTH FROM created_at) = :month', {
                        month: monthInNumber,
                    });
                }
            },
        });
        return new paginate_1.Pagination({
            results,
            total,
        });
    }
    async delete(id) {
        const story = await this.storyRepository.findOne(id);
        if (!story) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.NOT_FOUND,
                error: 'Story not found',
            }, common_1.HttpStatus.NOT_FOUND);
        }
        story.deletedAt = new Date();
        await this.storyRepository.save(story);
        return story;
    }
};
__decorate([
    (0, common_1.UseInterceptors)(common_1.ClassSerializerInterceptor),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [APIStory_dto_1.APIStoryDTO]),
    __metadata("design:returntype", Promise)
], StoryService.prototype, "add", null);
__decorate([
    (0, common_1.UseInterceptors)(common_1.ClassSerializerInterceptor),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, queryStories_dto_1.QueryStoriesDto]),
    __metadata("design:returntype", Promise)
], StoryService.prototype, "findAll", null);
__decorate([
    (0, common_1.UseInterceptors)(common_1.ClassSerializerInterceptor),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], StoryService.prototype, "delete", null);
StoryService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_2.InjectRepository)(story_entity_1.Story)),
    __metadata("design:paramtypes", [typeorm_1.Repository,
        axios_1.HttpService])
], StoryService);
exports.StoryService = StoryService;
//# sourceMappingURL=story.service.js.map