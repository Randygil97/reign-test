export declare class APIStoryDTO {
    story_id: string;
    created_at: Date;
    title: string;
    url: string;
    author: string;
    points: number;
    story_text: string;
    comment_text: string;
    num_comments: number;
    story_title: string;
    story_url: string;
    objectID: string;
    created_at_i: number;
    _tags: string[];
    constructor(story_id: string, created_at: Date, title: string, url: string, author: string, points: number, story_text: string, comment_text: string, num_comments: number, story_title: string, story_url: string, created_at_i: number, _tags: string[]);
}
