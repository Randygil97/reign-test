"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.APIStoryDTO = void 0;
class APIStoryDTO {
    constructor(story_id, created_at, title, url, author, points, story_text, comment_text, num_comments, story_title, story_url, created_at_i, _tags) {
        this.story_id = story_id;
        this.created_at = created_at;
        this.title = title;
        this.url = url;
        this.author = author;
        this.points = points;
        this.story_text = story_text;
        this.comment_text = comment_text;
        this.num_comments = num_comments;
        this.story_title = story_title;
        this.story_url = story_url;
        this.created_at_i = created_at_i;
        this._tags = _tags;
    }
}
exports.APIStoryDTO = APIStoryDTO;
//# sourceMappingURL=APIStory.dto.js.map