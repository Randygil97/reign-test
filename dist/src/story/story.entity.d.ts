import { Tag } from './tags.entity';
export declare class Story {
    objectID: string;
    deletedAt?: Date;
    created_at: Date;
    title: string;
    url: string;
    author: string;
    points: number;
    story_text: string;
    comment_text: string;
    num_comments: number;
    story_title: string;
    story_url: string;
    created_at_i: number;
    tags: Tag[];
}
