import { SuccessDto } from '../success.dto';
import { Pagination } from './../paginate';
import { DeleteStoryDTO } from './DeleteStory.dto';
import { QueryStoriesDto } from './queryStories.dto';
import { Story } from './story.entity';
import { StoryService } from './story.service';
export declare class StoryController {
    private storyService;
    constructor(storyService: StoryService);
    index(query: QueryStoriesDto): Promise<Pagination<Story>>;
    delete(param: DeleteStoryDTO): Promise<SuccessDto>;
}
