export declare class QueryStoriesDto {
    month?: string;
    title?: string;
    author?: string;
    tag?: string;
    limit?: number;
    page?: number;
}
