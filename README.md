# Reign Test

This is a Back End Developer Test provided by [reign](https://reign.cl)

## Up and running 

- Start the postgres and code container using docker, it runs on localhost:3000

```bash
$ docker-compose up -d 
```

## Use

- Navigate to http://localhost:3000/api/docs to see available endpoints

- Register using /api/users/register endpoint

- Log in using /api/login endpoint

- Use access_token provided by /api/login to authorize swagger for JWT blocked endpoints